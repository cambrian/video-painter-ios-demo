//
//  PainterUtility.m
//  VirtualPainter
//
//  Created by Joel Teply on 11/7/12.
//  Copyright (c) 2012 Joel Teply. All rights reserved.
//

#import "PainterUtility.h"
#import <HomeAugmentation/CBCoreData.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "SVProgressHUD.h"
#import "Brand.h"
#import "ColorCategory.h"
#import "Color.h"
#import "Color+Extended.h"


static NSTimeInterval _lastFavoritesFetch = 0;
static NSMutableArray *_favorites;

@implementation PainterUtility

+ (BOOL)isTouch:(UITouch *)touch withinView:(UIView *)view;
{
    CGPoint touchPoint = [touch locationInView:view];
    
    return touchPoint.x >= 0
    && touchPoint.y >= 0
    && touchPoint.x <= view.frame.size.width
    && touchPoint.y <= view.frame.size.height;
}

+ (UIImageView *)makeColorTexture:(UIImage *)texture withColor:(UIColor *)color withFrame:(CGRect)frame;
{
    UIImageView *view = [[UIImageView alloc] initWithFrame:frame];
    view.backgroundColor = color;
    
    if (texture) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            if (!view) {
                NSLog(@"Expired, no view or superiew");
                return;
            }
            
            UIImage *result = [self makeColorTextureImage:texture withColor:color withFrame:frame];
            
            if (!result) {
                NSLog(@"Expired");
                return;
            }
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                if (!view) return;
                view.image = result;
                view.contentMode = UIViewContentModeTopLeft;
                view.frame = frame;
            });
        });
    }
    
    return view;
}

+ (UIImage *)makeColorTextureImage:(UIImage *)texture withColor:(UIColor *)color withFrame:(CGRect)frame;
{
    CBImage *cbImage = [[CBImage alloc] initWithUIImage:texture];
    cbImage.canvasSize = IS_RETINA ? frame.size : CGSizeMake(frame.size.width * 2.0, frame.size.height * 2.0);
    
    [cbImage appendNewLayer];
    cbImage.topLayer.filter = CBFilterColor;
    [cbImage.topLayer fillWithColor:color];
    
    return cbImage.image;
}

+ (NSArray *) allBrands;
{
    // Brands
#ifdef LITE
    NSPredicate *query = [NSPredicate predicateWithFormat:@"ANY categories.pro_only==NO"];
#else
    NSPredicate *query = nil;
#endif
    NSArray *brands = [[CBCoreData sharedInstance] getRecordsForClass:[Brand class] predicate:query sortedBy:nil context:nil];
    
    NSMutableArray *filteredBrands = [NSMutableArray array];
    for (Brand *brand in brands) {
        if ([brand.categories count]) {
            [filteredBrands addObject:brand];
        }
    }
    
    return brands;
}

+ (NSArray *)allColors;
{
    return [[CBCoreData sharedInstance] getRecordsForClass:[Color class] predicate:nil sortedBy:nil context:nil];
}

+ (Color *)closestMatchForUIColor:(UIColor *)uiColor;
{
    NSArray *lotsOfColors = [NSArray array];
    int delta = 8;
    while ([lotsOfColors count] == 0 && delta < 100) {
        lotsOfColors = [self colorsMatchingUIColor:uiColor withinDelta:delta];
        delta = delta * 2;
    }
    return [self closestMatchForUIColor:uiColor withinColors:lotsOfColors excludingColors:NULL];
}

+ (Color *)closestMatchForUIColor:(UIColor *)uiColor brand:(Brand*)brand category:(ColorCategory*)category;
{
    if (!brand) {
        return [self closestMatchForUIColor:uiColor];
    }
    else if (category) {
        return [self closestMatchForUIColor:uiColor withinColors:category.colors excludingColors:NULL];
    }
    else {
        NSMutableArray * colors  = [NSMutableArray array];
        
        for (ColorCategory *category in brand.categories) {
            [colors addObjectsFromArray:category.colors.allObjects];
        }
        
        return [self closestMatchForUIColor:uiColor withinColors:colors excludingColors:NULL];
    }
}

+ (NSArray *)colorsMatchingUIColor:(UIColor *)uiColor withinDelta:(int)delta
{
    CGFloat r, g, b;
    [uiColor getRed:&r green:&g blue:&b alpha:nil];
    
    int red = 255.0 * r;
    if (red - delta < 0) red = delta;
    else if (red + delta > 255) red = 255 - delta;
    
    int green = 255.0 * g;
    if (green - delta < 0) green = delta;
    else if (green + delta > 255) green = 255 - delta;
    
    int blue = 255.0 * b;
    if (blue - delta < 0) blue = delta;
    else if (blue + delta > 255) blue = 255 - delta;
    
    NSMutableString *query = [NSMutableString stringWithString:@"(red > %i && red < %i\
                              && green > %i && green < %i\
                              && blue > %i && blue < %i)"];
    
#ifdef LITE
    [query appendString:@" AND category.pro_only==NO"];
#endif
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:query,
                              red - delta, red + delta,
                              green - delta, green + delta,
                              blue - delta, blue + delta];
    
    
    return [[CBCoreData sharedInstance] getRecordsForClass:[Color class] predicate:predicate sortedBy:nil context:nil];
}

+ (Color *)closestMatchForUIColor:(UIColor *)uiColor
                     withinColors:(id<NSFastEnumeration>)colors
                  excludingColors:(NSArray *)colorsToExclude;
{
    double bestDistance = DBL_MAX;
    Color *bestColor;
    for (Color *color in colors) {
        if (colorsToExclude && [colorsToExclude containsObject:color]) {
            continue;
        }
        double distance = [color distanceFromUIColor:uiColor];
        if (distance < bestDistance) {
            bestDistance = distance;
            bestColor = color;
        }
    }
    
    return bestColor;
}

+ (void) saveImageToPhotoAlbum:(UIImage *)image;
{
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    [library writeImageToSavedPhotosAlbum:[image CGImage]
                              orientation:(ALAssetOrientation)[image imageOrientation]
                          completionBlock:^(NSURL *assetURL, NSError *error){
                              if (!error) NSLog(@"Saved image to photo album");
                              else NSLog(@"ERROR: Failed to save image to photo album");
                          }];
    
}

+ (void) saveVideoToPhotoAlbum:(NSURL *)url;
{
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    [library writeVideoAtPathToSavedPhotosAlbum:url
                                completionBlock:^(NSURL *assetURL, NSError *error){
                                    if (!error) NSLog(@"Saved video to photo album");
                                    else NSLog(@"ERROR: Failed to save video to photo album");
                                    [[NSFileManager defaultManager] removeItemAtURL:url error:nil];
                                }];
}

+ (NSMutableArray *) favorites
{
    NSTimeInterval favoritesAge = [NSDate timeIntervalSinceReferenceDate] - _lastFavoritesFetch;
    if (favoritesAge > 30) {
        NSPredicate *query = [NSPredicate predicateWithFormat:@"favorite==1", INITIAL_COLOR_CATEGORY];
        _favorites = [NSMutableArray  arrayWithArray:[[CBCoreData sharedInstance] getRecordsForClass:[Color class] predicate:query sortedBy:nil context:nil]];
        _lastFavoritesFetch = [NSDate timeIntervalSinceReferenceDate];
    }
    return _favorites;
}

+ (void)popupWithStatus:(NSString *)status queue:(dispatch_queue_t)queue block:(void (^)())block;
{
    [SVProgressHUD showWithStatus:status];
    NSTimeInterval start = [NSDate timeIntervalSinceReferenceDate];
    dispatch_async(queue, ^{
        
        if (block) block();
        
        NSTimeInterval timeTaken = [NSDate timeIntervalSinceReferenceDate] - start;
        double delayInSeconds = (timeTaken < 1.0) ? 1.0 - timeTaken : 0.01;
        
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [SVProgressHUD dismiss];
        });
    });
}

static BOOL performingBlock;
+ (void) performBlock:(void (^)(void))block
            delayedBy:(double)delay
              onQueue:(dispatch_queue_t)queue
           usingTimer:(dispatch_source_t *)timer;
{
    if (performingBlock) {
        dispatch_async(queue, block);
        return;
    }
    
    //cancel existing
    if (*timer) {
        dispatch_source_cancel(*timer);
        *timer = 0;
    }
    
    *timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0));
    dispatch_source_set_timer(*timer,
                              dispatch_time(DISPATCH_TIME_NOW, delay * NSEC_PER_SEC),
                              DISPATCH_TIME_FOREVER, 0);
    dispatch_source_set_event_handler(*timer, ^{
        
        performingBlock = YES;

        dispatch_source_cancel(*timer);
        *timer = 0;
    
        dispatch_async(queue, ^{
            block();
            performingBlock = NO;
        });
    });
    
    //start it
    dispatch_resume(*timer);
}

+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    if (&NSURLIsExcludedFromBackupKey == nil) { // iOS < 5.0.1
        return false;
    }
    else { // iOS >= 5.1
        NSError *error = nil;
        [URL setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:&error];
        return error == nil;
    }
}

+ (void)setFavoriteColor:(Color *)color isFavorite:(BOOL)isFavorite;
{
    color.favorite = [NSNumber numberWithBool:isFavorite];
    [[[CBCoreData sharedInstance] managedObjectContext] save:nil];
    
    //have to do this, database delays
    if (isFavorite) {
        [_favorites addObject:color];
    } else {
        [_favorites removeObject:color];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:FavoritesChangedNotification
                                                        object:nil
                                                      userInfo:nil];
    
}

+ (NSString *)appStoreHTML
{
    NSMutableString * result = [NSMutableString stringWithFormat:@"<i>Sent from %@ for ", APP_NAME];
    if (IS_IPAD) {
        [result appendString:@"iPad"];
    } else {
        [result appendString:@"iPhone"];
    }
    [result appendString:@"</i><br />"];
    [result appendFormat:@"<a href=\"%@\">Click here for more</a>", PRO_URL];
    return result;
}

+ (UIImageView *) setupTutorialStep:(UIView *)view
                              above:(BOOL)above
                           centered:(BOOL)centered;
{
    float angle = above ? 0 : M_PI;
    
    UIImage *arrowImage = [UIImage imageNamed:@"arrow-up.png"];
    
    float xOffset = centered ? view.frame.size.width / 2 - arrowImage.size.width / 2 : -5;
    float yOffset = above ?  - arrowImage.size.height - 5 : view.frame.size.height + 5;
    
    CGPoint arrowPosition = CGPointMake(view.frame.origin.x + xOffset, view.frame.origin.y + yOffset);
    
    UIImageView *arrow = [[UIImageView alloc] initWithImage:arrowImage];
    arrow.frame = CGRectSetOrigin(arrowPosition, arrow.frame);
    if (angle) arrow.transform = CGAffineTransformMakeRotation(angle);
    [view.superview insertSubview:arrow belowSubview:view];
    
    arrow.hidden = view.hidden;
    return arrow;
}


@end
