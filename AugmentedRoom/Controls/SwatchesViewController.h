//
//  SwatchesViewController.h
//  Home Decorator
//
//  Created by Joel Teply on 1/3/12.
//  Copyright (c) 2012 Digital Rising LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "ColorCategory.h"
#import "Controls.h"

@class TapScrollView, Color;

@interface SwatchesViewController : BaseViewController <UIScrollViewDelegate>

@property(assign) id<SelectColorDelegate> delegate;
@property (retain, nonatomic) IBOutlet TapScrollView *scrollView;
@property (retain, nonatomic) IBOutlet UIView *colorView;
@property (retain, nonatomic) ColorCategory *colorCategory;
@property (retain, nonatomic) Color *selectedColor;

- (void) refresh;

@end
