//
//  SplashViewController.m
//  AugmentedRoom
//
//  Created by Joel Teply on 10/19/12.
//  Copyright (c) 2012 Joel Teply. All rights reserved.
//

#import "SplashViewController.h"
#import "MainViewController.h"

@interface SplashViewController ()

@end

@implementation SplashViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void) viewDidAppear:(BOOL)animated
{
    int64_t delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self performSegueWithIdentifier:@"ShowMainView" sender:self];
    });
}


@end
