//
//  CBDefines.h
//  HomeAugmentationFramework
//
//  Created by Joel Teply on 6/9/14.
//  Copyright (c) 2014 Joel Teply. All rights reserved.
//

#if DEBUG

#else

#define NSLog(format,...)

#endif

#define START_TIMER(label) NSString *timeLogLabel=label; NSTimeInterval startTime = [NSDate timeIntervalSinceReferenceDate]; NSTimeInterval lastTime = startTime; NSLog(@"%@ STARTED", timeLogLabel);
#define LOG_TIMER_POINT(label) NSLog(@"%@.%@ took %f, total: %f seconds", timeLogLabel, label,  ([NSDate timeIntervalSinceReferenceDate] - lastTime), ([NSDate timeIntervalSinceReferenceDate] - startTime)); lastTime = [NSDate timeIntervalSinceReferenceDate];
#define LOG_TIMER LOG_TIMER_POINT(@"")

#define RESAMPLE_OVERLAY 1

#define IS_RETINA ([[UIScreen mainScreen] scale] > 1.0)
#define IS_MULTICORE ([CBThreading isMultiCore])
#define IS_WIDESCREEN ( fabs( ( double )[ [UIScreen mainScreen] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_FAST_DEVICE IS_MULTICORE

#define IS_IPHONE ( [ [ [ UIDevice currentDevice ] model ] isEqualToString: @"iPhone" ] )
#define IS_IPOD   ( [ [ [ UIDevice currentDevice ] model ] isEqualToString: @"iPod touch" ] )
#define IS_IPHONE_5 ( IS_IPHONE && IS_WIDESCREEN )
#define IS_IPAD ( [[[ UIDevice currentDevice ] model] hasPrefix:@"iPad"] )

#define HAS_CAMERA ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerSourceTypeCamera])