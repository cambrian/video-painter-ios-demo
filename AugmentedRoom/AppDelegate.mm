//
//  AppDelegate.m
//  AugmentedRoom
//
//  Created by Joel Teply on 10/12/12.
//  Copyright (c) 2012 Joel Teply. All rights reserved.
//

#import "AppDelegate.h"
#import <HomeAugmentation/HomeAugmentation.h>
#import <HomeAugmentation/CBCoreData.h>
#import "MainViewController.h"
#import "Color.h"
#import "PainterUtility.h"

#if RATINGS_ENABLED
#import "Appirater.h"
#endif

@interface AppDelegate() {

    BOOL _isFirstRun;
}

@end

@implementation AppDelegate

@synthesize navigationController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [CBLicensing enableWithKey:LICENSE_KEY];
    
    [[CBCoreData sharedInstance] setWillUpgradeBlock:^(NSManagedObjectContext *oldContext, NSManagedObjectContext *newContext){
        NSPredicate *query = [NSPredicate predicateWithFormat:@"favorite==1", INITIAL_COLOR_CATEGORY];
        NSArray *favorites = [NSMutableArray  arrayWithArray:[[CBCoreData sharedInstance] getRecordsForClass:[Color class]
                                                                                          predicate:query
                                                                                           sortedBy:nil
                                                                                            context:oldContext]];
        NSLog(@"Got %li favorites", [favorites count]);
        
        int transfered = 0;
        for (Color *oldFavorite in favorites) {
            Color *newFavorite = (Color *) [newContext objectWithID:oldFavorite.objectID];
            if (newFavorite) {
                newFavorite.favorite = oldFavorite.favorite;
                transfered ++;
            }
        }
        [newContext save:nil];
        NSLog(@"Updated %i of %i favorites", transfered, [favorites count]);

//        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Upgrade Successful"
//                                                          message:@"Database was upgraded"
//                                                         delegate:self
//                                                cancelButtonTitle:@"OK"
//                                                otherButtonTitles:nil];
//        [message show];

    }];
    
    [[CBCoreData sharedInstance] setDatabaseName:DATABASE_NAME];
    
    [[CBCoreData sharedInstance] managedObjectContext];
    [PainterUtility addSkipBackupAttributeToItemAtURL:[[CBCoreData sharedInstance] databaseUrl]];
    
    _isFirstRun = YES;
    
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    MainViewController *mainViewController = [[MainViewController alloc] initWithNibName:nil bundle:nil];
    
    self.navigationController = [[UINavigationController alloc] initWithRootViewController:mainViewController];

    self.window.rootViewController = self.navigationController;
    [self.window makeKeyAndVisible];
    
#if RATINGS_ENABLED
#if LITE
    [Appirater setAppId:@"583502326"];
#else
    [Appirater setAppId:@"581539953"];
#endif
    
    [Appirater setDaysUntilPrompt:1];
    [Appirater setUsesUntilPrompt:10];
    [Appirater setSignificantEventsUntilPrompt:-1];
    [Appirater setTimeBeforeReminding:2];
#if DEBUG
    [Appirater setDebug:NO];
#endif
    
    [Appirater appLaunched:YES];
#endif
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
    [self.window.rootViewController viewWillDisappear:YES];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [self.window.rootViewController viewDidDisappear:YES];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
#if RATINGS_ENABLED
    [Appirater appEnteredForeground:YES];
#endif
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

    if (!_isFirstRun) {
        [self.window.rootViewController viewDidAppear:YES];
    }
    
    _isFirstRun = NO;
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
