//
//  PaintSwatch.h
//  Home Painter
//
//  Created by Joel Teply on 1/26/12.
//  Copyright (c) 2012 Digital Rising LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Color;

@interface PaintSwatch : UIView

@property (retain, nonatomic) Color *color;
@property (assign, nonatomic) BOOL selected;

@end
