//
//  ImagePainter.m
//  Paint Harmony
//
//  Created by Joel Teply on 12/8/13.
//
//

#import "ImagePainter.h"

@implementation ImagePainter

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
