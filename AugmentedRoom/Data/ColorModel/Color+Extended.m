//
//  Color+Extended.m
//  ColorParser
//
//  Created by Joel Teply on 1/25/12.
//  Copyright (c) 2012 Digital Rising LLC. All rights reserved.
//

#import "Color+Extended.h"

@implementation Color (Extended)

@dynamic uiColor;

- (UIColor *)uiColor;
{
    return [UIColor colorWithRed:[self.red doubleValue]/255.0f
                           green:[self.green doubleValue]/255.0f
                            blue:[self.blue doubleValue]/255.0f
                           alpha:1.0];
}

- (void) setUiColor:(UIColor *)uiColor;
{
    const CGFloat *components = uiColor ? CGColorGetComponents(uiColor.CGColor)
        : CGColorGetComponents([UIColor clearColor].CGColor);
    
    self.red = [NSNumber numberWithFloat:components[0] * 255.0f];
    self.green = [NSNumber numberWithFloat:components[1] * 255.0f];
    self.blue = [NSNumber numberWithFloat:components[2] * 255.0f];
}

#ifdef CAMBRIAN
- (double) distanceFromUIColor:(UIColor *)color;
{
    return [CBColoring distance:color fromColor:self.uiColor];
}
#endif

@end
