//
//  ViewController.m
//  AugmentedRoom
//
//  Created by Joel Teply on 10/12/12.
//  Copyright (c) 2012 Joel Teply. All rights reserved.
//

#import "MainViewController.h"
#import "SideBarViewController.h"
#import "Brand.h"
#import "ColorCategory.h"
#import "Color.h"
#import "Color+Extended.h"
#import "ColorViewController.h"
#import <HomeAugmentation/CBCoreData.h>
#import "PainterUtility.h"
#import "FCColorPickerViewController.h"
#import "HarmonyViewController.h"
#import "SettingsModel.h"
#import "ColorScroller.h"
#import "SwatchesViewController.h"
#import "SVProgressHUD.h"
#import "ImagePainter.h"


#include <queue>

#define FAVORITES_ALERT @"Favorites"
#define SAVE_CHANGES_ALERT @"Save Changes"

typedef enum
{
	PickerModeSwatches,
    PickerModePicker,
    PickerModeFavorites,
}  PickerMode;

#define UI_BORDER_SIZE (IS_IPAD ? 40 : 17)
#define SWATCHES_BUTTON_MARGIN 10
#define AUTOCOMMIT_SECONDS 4.0

static dispatch_queue_t _drawing_queue;
static dispatch_queue_t drawing_queue() {
//    return dispatch_get_main_queue();
    
    if (_drawing_queue == NULL) {
        _drawing_queue = dispatch_queue_create("com.cambriantech.virtual_painter.drawing_queue", 0);
    }
    
    return _drawing_queue;
}

@interface MainViewController () <CBVideoDeviceDelegate> {

    BOOL _isFirstRun;
    CGPoint _controlsOrigin;
    CGPoint _swatchButtonOrigin;
    BOOL _subviewPending;

    std::queue<CGPoint>_toolPointQueue;
    dispatch_source_t _color_finder_timer, _still_mask_timer, _recording_timer;
    dispatch_source_t _block_timer;

    float _bannerHeight;
    
    UIPopoverController *_popover;
    BOOL _wasRunning;
}

@property (strong, nonatomic) CBDrawerController *drawer;
@property (strong, nonatomic) CBVideoPainter *videoPainter;

@property (strong, nonatomic) SideBarViewController *sideDrawer;

@property (strong, nonatomic) SwatchesViewController *swatches;
@property (strong, nonatomic) FCColorPickerViewController *picker;
@property (strong, nonatomic) ColorScroller *favorites;

@property (retain, nonatomic) Color *color;
@property (assign, nonatomic) UIColor *uiColor;
@property (assign, nonatomic) PickerMode pickerMode;
@property (assign, nonatomic) BOOL recording;
@property (assign, nonatomic) int recordingSeconds;
@property (retain, nonatomic) ADBannerView *banner;
@property (assign, nonatomic) BOOL unsavedWork;
@property (assign, nonatomic) BOOL loadingSubview;

@property dispatch_source_t commitTimer;
@property BOOL commitTimingOut;

@end

@implementation MainViewController

@synthesize sideDrawer = _sideDrawer;
@synthesize swatches = _swatches;
@synthesize picker = _picker;
@synthesize videoPainter = _videoPainter;
@synthesize color = _color;
@synthesize unsavedWork = _unsavedWork;

@dynamic uiColor;

@synthesize recording = _recording;
@synthesize recordingSeconds = _recordingSeconds;
@synthesize loadingSubview = _loadingSubview;

- (void)dealloc
{
    if (_drawing_queue)
    {
        dispatch_release(_drawing_queue);
    }
    busyUpdatingColor = NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _isFirstRun = YES;
    [self.navigationController setNavigationBarHidden:YES];
    
    __unsafe_unretained MainViewController *weakSelf = self;
    
    self.videoPainter = [[CBVideoPainter alloc] initWithCameraAtPosition:AVCaptureDevicePositionBack delegate:self];
    
    self.addLayerButton.enabled = NO;
    
    [self.imagePainter setHistoryChangedBlock:^{
        [weakSelf updateTools];
        weakSelf.addLayerButton.enabled = weakSelf.imagePainter.canAppendNewLayer;
        if (weakSelf.imagePainter.canStepBackward) {
            weakSelf.unsavedWork = YES;
        }
    }];
    
    [self.imagePainter setStartedToolBlock:^(ToolMode toolMode) {
        if (toolMode == ToolModeFill && !IS_MULTICORE) {
            [SVProgressHUD showWithStatus:@"Painting"];
        }
    }];
    
    [self.imagePainter setFinishedToolBlock:^(ToolMode toolMode) {
        [SVProgressHUD dismiss];
        [weakSelf.imagePainter commitChanges];
    }];
    
    self.brandCheckbox.selected = [[SettingsModel getInstance] matchPickerColors];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    //cv::Mat test = cv::Mat();
    self.sideDrawer = [[SideBarViewController alloc] initWithNibName:nil
                                                              bundle:nil];
    self.sideDrawer.delegate = self;
    self.sideDrawer.view.frame = CGRectSetX(0, self.sideDrawer.view.frame);
    self.sideDrawer.view.frame = CGRectSetH(self.view.frame.size.height, self.sideDrawer.view.frame);
    [self.view insertSubview:self.sideDrawer.view atIndex:0];
    self.sideDrawer.view.hidden = YES;
    
    self.swatches = [[SwatchesViewController alloc] initWithNibName:nil bundle:nil];
    self.swatches.delegate = self;
    [self.videoView addSubview:self.swatches.view];
    
    self.swatchesArrowDown.transform = CGAffineTransformMakeRotation(M_PI);
    self.swatchesArrowDown.hidden = YES;
    
    self.colorLabel.hidden = YES;
    self.pickerInfoView.hidden = YES;
    self.pickerInfoView.frame = CGRectSetY(self.view.frame.size.height, self.pickerInfoView.frame);
    
    self.picker = [[FCColorPickerViewController alloc] initWithNibName:nil bundle:nil];
    self.picker.delegate = self;
    [self.videoView insertSubview:self.picker.view belowSubview:self.pickerInfoView];
    
    self.stillControls.hidden = YES;
    self.brushSizeImage.hidden = YES;
    
    self.closeButton.hidden = YES;
    
    self.favorites = [[ColorScroller alloc] initWithDelegate:self
                                                       title:@"Favorites" colors:nil texture:nil
                                                        size:IS_IPAD ? CGSizeMake(200,200) : (IS_WIDESCREEN ? CGSizeMake(100,100) : CGSizeMake(80,80))];
    
    [self.videoView addSubview:self.favorites.view];
    
//    self.coverView = [[UIView alloc] initWithFrame:self.videoView.frame];
//    self.coverView.hidden = YES;
//    [self.view addSubview:self.coverView];
    
    self.drawer = [[CBDrawerController alloc] initWithDelegate:self
                                                   primaryView:self.videoView
                                                   sidebarView:self.sideDrawer.view
                                                   shadowImage:[UIImage imageNamed:@"vertical_shadow.png"]];
    
#if DEBUG && LITE
    double delayInSeconds = 5.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        if (self.banner.hidden) {
            [self bannerViewDidLoadAd:self.banner];
        }
    });
#endif
    
#if LITE
    self.videoButton.hidden = YES;
#endif
}

- (void) pickRandomColor
{
    NSArray *allColors = [PainterUtility allColors];
    
    srand(time(NULL));
    int randomIndex = rand() % [allColors count];
    
    Color *initialColor = [allColors objectAtIndex:randomIndex];
    [self SelectColor_PickedColor:initialColor uiColor:initialColor.uiColor];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (!_wasRunning && ([self isStillMode] || self.modalViewController || self.drawer.isOpen)) {
        NSLog(@"isStillMode=%i, modalViewController=%i, drawerIsOpen=%i",
              [self isStillMode], self.modalViewController!=nil, self.drawer.isOpen);
        return;
    }
    
    _wasRunning = NO;//forget it
    
    if (self.pickerMode == PickerModeFavorites) {
        [self refreshFavorites];
    }
//    ColorCategory *category = [[CBCoreData sharedInstance] getFirstRecordForClass:[ColorCategory class]
//                                                                        predicate:[NSPredicate predicateWithFormat:@"name==%@", INITIAL_COLOR_CATEGORY]
//                                                                         sortedBy:nil];
//    [self loadCategory:category];
    
    if (_subviewPending) return;
    
    if (!self.color) {
        [self pickRandomColor];
    }
    
    if (!self.videoPainter.isRunning) {
        NSLog(@"Start Running");
        __unsafe_unretained MainViewController *weakSelf = self;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [weakSelf.videoPainter startRunning];
        });
    }
    
//    if (_isFirstRun) {
//        [self.painter startRunning];
//        _isFirstRun = NO;
//    } else {
//        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC));
//        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//            [self.painter startRunning];
//        });
//    }
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    _wasRunning |= self.videoPainter.isRunning;
    [self.videoPainter stopRunning];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    _wasRunning |= self.videoPainter.isRunning;
    [self.videoPainter stopRunning];
}

- (void) autolayoutCompleted;
{
//    if (self.topBarImage) {
//        self.topBarImage.frame = CGRectMake(0, 50, 768, 40);
//    }
    self.statusLabel.hidden = YES;
    self.videoOutput.frame = self.videoView.frame;
    
    
//    self.coverView.frame = CGRectMake(0, self.topControls.frame.size.height,
//                                      self.videoView.frame.size.width,
//                                      self.videoView.frame.size.height - self.topControls.frame.size.height);
    _bannerHeight = 0;
    
#if LITE
    
    self.banner = [[ADBannerView alloc] initWithFrame:CGRectZero];
    self.banner.delegate = self;
    self.banner.hidden = YES;
    self.banner.frame = CGRectSetY(self.videoView.frame.size.height, self.banner.frame);
    [self.videoView addSubview:self.banner];
    
#endif
    
    self.imagePainter.hidden = YES;
    self.imagePainter.frame = self.videoOutput.frame;
    
    self.imagePainter.paintColor = self.uiColor;
    self.floodOptions.hidden = YES;
    self.addLayerButton.hidden = YES;
    
    [self.imagePainter setStartedToolBlock:^(ToolMode toolMode) {
        if (toolMode == ToolModeFill) {
            if (toolMode == ToolModeFill && !IS_MULTICORE) {
                [SVProgressHUD showWithStatus:@"Painting"];
            }
        }
    }];
    
    [self.imagePainter setFinishedToolBlock:^(ToolMode toolMode) {
        if (toolMode == ToolModeFill) {
            [SVProgressHUD dismiss];
        }
    }];

    //self.videoOutput.backgroundColor = [UIColor redColor];
    
//    NSLog(@"self.videoOutput.frame.size.height = %f", self.videoOutput.frame.size.height);
    self.videoPainter.output = self.videoOutput;
    
    //self.view.frame = CGRectSetH(548, self.view.frame);
    //NSLog(@"self.view.frame.size.height=%f", self.view.frame.size.height);
    
    if (!_controlsOrigin.x) {
        _controlsOrigin = CGPointMake(self.cameraControls.frame.origin.x
                                      , self.view.frame.size.height
                                      - self.cameraControls.frame.size.height
                                      - UI_BORDER_SIZE
                                      - _bannerHeight);
    }
    
    [self controlsOriginChanged];
    
    self.swatches.view.frame = CGRectSetY(self.view.frame.size.height, self.swatches.view.frame);
    self.swatches.view.frame = CGRectSetW(self.view.frame.size.width, self.swatches.view.frame);
    self.swatches.view.frame = CGRectSetX(0, self.swatches.view.frame);
    
    self.picker.view.frame = CGRectSetY(self.view.frame.size.height, self.picker.view.frame);
    self.picker.view.frame = CGRectSetW(self.view.frame.size.width, self.picker.view.frame);
    self.picker.view.frame = CGRectSetX(0, self.picker.view.frame);
    self.picker.view.hidden = YES;
    
    
//    NSLog(@"%fx%f (%f, %f)",
//          self.picker.swatch.frame.size.width,
//          self.picker.swatch.frame.size.height,
//          self.picker.swatch.frame.origin.x,
//          self.picker.swatch.frame.origin.y);
    
    self.favorites.view.frame = CGRectSetW(self.view.frame.size.width, self.favorites.view.frame);
    self.favorites.view.frame = CGRectSetY(self.videoView.frame.size.height, self.favorites.view.frame);
    
    [self.videoPainter startRunning];
}

- (void) controlsOriginChanged
{
    if ([self isStillMode]) {
        //drop out camera
        self.cameraControls.frame = CGRectSetY(self.videoView.frame.size.height
                                               - _bannerHeight, self.cameraControls.frame);
        self.stillControls.frame = CGRectSetY(self.videoView.frame.size.height
                                              - self.stillControls.frame.size.height
                                              - _bannerHeight, self.stillControls.frame);
    } else {
        self.cameraControls.frame = CGRectSetY(_controlsOrigin.y,
                                               self.cameraControls.frame);
        self.stillControls.frame = CGRectSetY(self.videoView.frame.size.height
                                              - _bannerHeight, self.stillControls.frame);
    }
    
    
    _swatchButtonOrigin = CGPointMake(self.swatchesContainer.frame.origin.x,
                                      _controlsOrigin.y
                                      + (self.cameraControls.frame.size.height
                                         - self.swatchesContainer.frame.size.height) / 2);
    
    self.swatchesContainer.frame = CGRectSetY(_swatchButtonOrigin.y,
                                              self.swatchesContainer.frame);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setVideoView:nil];
    [self setStatusLabel:nil];
    [self setColorLabel:nil];
    [self setTopControls:nil];
    [self setColorButtonView:nil];
    [self setSwatchesColorView:nil];
    [self setSwatchesContainer:nil];
    [self setSwatchesArrowUp:nil];
    [self setSwatchesArrowDown:nil];
    [self setCameraControls:nil];
    [self setStillControls:nil];
    [self setVideoOutput:nil];
    [self setPickerInfoView:nil];
    [self setBrandCheckbox:nil];
    [self setPickerInfoLabel:nil];
    [self setUndoButton:nil];
    [self setPaintButton:nil];
    [self setEraseButton:nil];
    [self setBrushSizeSlider:nil];
    [self setBrushSizeImage:nil];
    [self setCloseButton:nil];
    [self setBucketButton:nil];
    [self setCameraButton:nil];
    [self setVideoButton:nil];
    [self setVideoTimeLabel:nil];
    [self setTopBarImage:nil];
    [super viewDidUnload];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    self.sideDrawer.view.hidden = NO;
    UITouch *touch = [touches anyObject];
    
    if (!self.drawer.isOpen && [self isStillMode]) {
        CGPoint touchPointInTools = [touch locationInView:self.stillControls];
        if (touchPointInTools.y > 0) {
            return;
        }
        [self.imagePainter touchesBegan:touches withEvent:event];
    } else {
        [self.drawer touchesBegan:touches withEvent:event];
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
 
    NSLog(@"drawer is %@", self.drawer.isOpen ? @"Open" : @"Closed");
    UITouch *touch = [touches anyObject];
    if (!self.drawer.isOpen && [self isStillMode]) {
        CGPoint touchPointInTools = [touch locationInView:self.stillControls];
        if (touchPointInTools.y > 0) {
            return;
        }
        [self.imagePainter touchesMoved:touches withEvent:event];
    } else {
        [self.drawer touchesMoved:touches withEvent:event];
    }
}

- (void) touchesEnded: (NSSet *) touches withEvent: (UIEvent *) event
{
    UITouch *touch = [touches anyObject];
    if (!self.drawer.isOpen && [self isStillMode]) {
        CGPoint touchPointInTools = [touch locationInView:self.stillControls];
        if (touchPointInTools.y > 0) {
            return;
        }
        [self.imagePainter touchesEnded:touches withEvent:event];
    } else {
        [self.drawer touchesEnded:touches withEvent:event];
    }
}

#if DEBUG & 0
- (void) imageUpdated;
{
    self.statusLabel.hidden = NO;
    self.statusLabel.text = self.videoPainter.status;
}
#endif

- (void) setColor:(Color *)color;
{
    _color = color;
    if (color) {
        __unsafe_unretained MainViewController *weakSelf = self;
        if (self.pickerMode == PickerModeSwatches && color.category != self.swatches.colorCategory) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf loadCategory:color.category];
            });
        }
        
        if (self.swatches.selectedColor != color) {
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelf.swatches.selectedColor = self.color;
            });
        }
        
        self.colorLabel.hidden = NO;
        self.colorLabel.text = [NSString stringWithFormat:@"%@ - %@", color.category.brand.name, color.name];
        [self setUIColor:color.uiColor];
    } else {
        self.colorLabel.hidden = YES;
    }
}

- (UIColor *) uiColor
{
    return self.colorButtonView.backgroundColor;
}

- (void) setUIColor:(UIColor *)uiColor;
{
    self.videoPainter.paintColor = uiColor;
    self.imagePainter.paintColor = uiColor;
    
    self.picker.color = uiColor;
    self.colorButtonView.backgroundColor = uiColor;
    self.swatchesColorView.backgroundColor = uiColor;
}

- (void) loadCategory:(ColorCategory *)colorCategory;
{
    self.swatches.colorCategory = colorCategory;
    self.pickerInfoLabel.text = [NSString stringWithFormat:@"%@ - %@",
                                    colorCategory.brand.name, colorCategory.name];
}


#pragma mark -
#pragma mark IBActions

- (IBAction)drawerClicked:(id)sender {
    [self.drawer toggleDrawer:!self.drawer.isOpen];
}

- (IBAction)colorButtonClicked:(id)sender {
    
    if (!self.color) return;
    
    [self displayColorDetails:self.color cameFromSelf:YES];
}

- (void)displayColorDetails:(Color *)color cameFromSelf:(BOOL)cameFromSelf;
{
    _subviewPending = !cameFromSelf;
    ColorViewController *colorVC = [[ColorViewController alloc] initWithNibName:nil bundle:nil];
    colorVC.parent = self;
    
    colorVC.color = color;
    colorVC.delegate = self;
    
    colorVC.modalPresentationStyle = UIModalPresentationPageSheet;
    colorVC.modalTransitionStyle = cameFromSelf ? UIModalTransitionStyleFlipHorizontal : UIModalTransitionStyleCoverVertical;
    
    if (self.videoPainter.isRunning) {        
        [self.videoPainter stopRunning];
    }
    
    [self dismissViewControllerAnimated:NO completion:^{
        _subviewPending = NO;
    }];
    
    [self presentViewController:colorVC animated:cameFromSelf completion:NULL];
}

- (BOOL)swatchesIsOpen
{
    return self.swatches.view.frame.origin.y < self.videoView.frame.size.height - _bannerHeight;
}

- (void)infoViewVisibility:(PickerMode)mode
{
    UIView *view;
    
    switch (mode) {
        case PickerModeSwatches:
            view = self.swatches.view;
            break;
        case PickerModePicker:
            view = self.picker.view;
            break;
        case PickerModeFavorites:
            view = self.favorites.view;
            break;
        default:
            break;
    }
    
    self.brandCheckbox.hidden = mode != PickerModePicker;
    self.pickerInfoLabel.hidden = mode != PickerModeSwatches;
    
    if (view.hidden) {
         self.pickerInfoView.hidden = (mode == PickerModeFavorites);
    }
    else {
        int height = 0;

        if (!self.pickerInfoLabel.hidden) {
            self.pickerInfoLabel.frame = CGRectSetY(height, self.pickerInfoLabel.frame);
            height += 30;
        }
        if (!self.brandCheckbox.hidden) {
            self.brandCheckbox.frame = CGRectSetY(height, self.brandCheckbox.frame);
            height += 30;
        }
        
        self.pickerInfoView.frame = CGRectSetH(height, self.pickerInfoView.frame);
        self.pickerInfoView.frame = CGRectSetY(view.frame.origin.y - height, self.pickerInfoView.frame);
        self.pickerInfoView.hidden = !height;
    }
    
}

- (void)openSwatches:(BOOL)open
{
    if (open) {
        [self openPicker:NO];
        [self openFavorites:NO];
    }
    
    self.swatchesColorView.hidden = YES;
    self.swatches.view.hidden = NO;
    
    [UIView animateWithDuration:0.25 animations:^{
        if (!open) {
            //close it
            self.swatches.view.frame = CGRectSetY(60 + self.view.frame.size.height - _bannerHeight, self.swatches.view.frame);
            self.swatchesContainer.frame = CGRectSetY(_swatchButtonOrigin.y,
                                                      self.swatchesContainer.frame);
            self.swatchesArrowUp.hidden = NO;
        } else {
            //open it
            self.swatches.view.frame = CGRectSetY(self.view.frame.size.height
                                                  - self.swatches.view.frame.size.height
                                                  - _bannerHeight,
                                                  self.swatches.view.frame);
            self.swatchesContainer.frame = CGRectSetY(self.swatches.view.frame.origin.y
                                                      - self.swatchesContainer.frame.size.height - SWATCHES_BUTTON_MARGIN,
                                                      self.swatchesContainer.frame);
            self.swatchesArrowUp.hidden = YES;
        }
        self.swatchesArrowDown.hidden = !self.swatchesArrowUp.hidden;
        
        [self infoViewVisibility:PickerModeSwatches];
    } completion:^(BOOL finished) {
        self.swatches.view.hidden = !open;
        [self infoViewVisibility:PickerModeSwatches];

        if (!IS_MULTICORE) {
            if (open) {
                self.videoPainter.throttled = YES;
            } else {
                self.videoPainter.throttled = NO;
            }
        }
    }];
}

- (BOOL)pickerIsOpen
{
    return self.picker.view.frame.origin.y < self.videoView.frame.size.height - _bannerHeight;
}

- (void)openPicker:(BOOL)open
{
    if (open) {
        [self openSwatches:NO];
        [self openFavorites:NO];
    }
    
    self.picker.view.hidden = NO;
    self.swatchesColorView.hidden = YES;
    
    [UIView animateWithDuration:0.25 animations:^{
        if (!open) {
            //close it
            self.picker.view.frame = CGRectSetY(60 + self.view.frame.size.height - _bannerHeight,
                                                self.picker.view.frame);
            self.swatchesContainer.frame = CGRectSetY(_swatchButtonOrigin.y,
                                                      self.swatchesContainer.frame);
            self.swatchesArrowUp.hidden = NO;

        } else {
            //open it
            self.picker.view.frame = CGRectSetY(self.view.frame.size.height
                                                  - self.picker.view.frame.size.height
                                                  - _bannerHeight,
                                                  self.picker.view.frame);
            self.swatchesContainer.frame = CGRectSetY(self.picker.view.frame.origin.y
                                                      - self.swatchesContainer.frame.size.height - SWATCHES_BUTTON_MARGIN,
                                                      self.swatchesContainer.frame);

            self.swatchesArrowUp.hidden = YES;
            self.swatchesColorView.hidden = NO;
        }
        self.swatchesArrowDown.hidden = !self.swatchesArrowUp.hidden;
        [self infoViewVisibility:PickerModePicker];
    } completion:^(BOOL finished) {
        self.picker.view.hidden = !open;
        [self infoViewVisibility:PickerModePicker];
        self.swatchesColorView.hidden = !open;
        
        if (!IS_MULTICORE) {
            if (open) {
                self.videoPainter.throttled = YES;
            } else {
                self.videoPainter.throttled = NO;
            }
        }
    }];
}

- (void)refreshFavorites
{
    __unsafe_unretained  MainViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf.favorites.colors removeAllObjects];
        [weakSelf.favorites.colors addObjectsFromArray:[PainterUtility favorites]];
        [weakSelf.favorites refresh];
    });
}

- (BOOL)favoritesIsOpen
{
    return self.favorites.view.frame.origin.y < self.videoView.frame.size.height - _bannerHeight;
}

- (void)openFavorites:(BOOL)open
{
    if (open) {
        [self openPicker:NO];
        [self openSwatches:NO];
    }
    
    self.favorites.view.hidden = NO;
    
    [UIView animateWithDuration:0.25 animations:^{
        if (!open) {
            //close it
            self.favorites.view.frame = CGRectSetY(60 + self.view.frame.size.height - _bannerHeight,
                                                   self.favorites.view.frame);
            self.swatchesContainer.frame = CGRectSetY(_swatchButtonOrigin.y,
                                                      self.swatchesContainer.frame);
            self.swatchesArrowUp.hidden = NO;
        } else {
            //open it
            self.favorites.view.frame = CGRectSetY(self.view.frame.size.height
                                                  - self.favorites.view.frame.size.height
                                                  - _bannerHeight,
                                                  self.favorites.view.frame);
            
            self.swatchesContainer.frame = CGRectSetY(self.favorites.view.frame.origin.y
                                                      - self.swatchesContainer.frame.size.height - SWATCHES_BUTTON_MARGIN,
                                                      self.swatchesContainer.frame);
            self.swatchesArrowUp.hidden = YES;
        }
        self.swatchesArrowDown.hidden = !self.swatchesArrowUp.hidden;
        [self infoViewVisibility:PickerModeFavorites];
    } completion:^(BOOL finished) {
        self.favorites.view.hidden = !open;
        [self infoViewVisibility:PickerModeFavorites];
        
        if (!IS_MULTICORE) {
            if (open) {
                self.videoPainter.throttled = YES;
            } else {
                self.videoPainter.throttled = NO;
            }
        }
    }];
}


- (BOOL) isStillMode
{
    return !self.stillControls.hidden;
}

- (void)toggleStillMode:(BOOL)show;
{
    self.imagePainter.toolMode = ToolModeFill;
    [self updateTools];
    
    self.cameraControls.hidden = NO;
    self.stillControls.hidden = NO;
    self.imagePainter.hidden = !show;
    
    self.closeButton.hidden = !show;
    self.videoOutput.hidden = show;
    
    self.addLayerButton.hidden = !show;
    
    //drop old out
    [UIView animateWithDuration:0.15 animations:^{
        if (show) {
            //drop out camera
            self.cameraControls.frame = CGRectSetY(self.view.frame.size.height
                                                   - _bannerHeight, self.cameraControls.frame);
        } else {
            //drop out tools
            self.stillControls.frame = CGRectSetY(self.view.frame.size.height, self.stillControls.frame);
        }
    } completion:^(BOOL finished) {
        //pop new in
        [UIView animateWithDuration:0.15 animations:^{
            if (show) {
                //pop in tools
                self.stillControls.frame = CGRectSetY(self.videoView.frame.size.height
                                                      - self.stillControls.frame.size.height
                                                       - _bannerHeight, self.stillControls.frame);
            } else {
                //pop camera in
                self.cameraControls.frame = CGRectSetY(_controlsOrigin.y, self.cameraControls.frame);
            }
        } completion:^(BOOL finished) {
            NSLog(@"Still Controls = %f", self.stillControls.frame.origin.y);
            if (show) {
                [self.videoPainter stopRunning];
            } else {
                [self.videoPainter startRunning];
            }
            self.cameraControls.hidden = show;
            self.stillControls.hidden = !show;
        }];
    }];
    
    
}

- (void)toggleVideoRecording:(BOOL)record
{
    if (record) {
        //start recording
        self.recording = YES;
        self.cameraButton.hidden = YES;
        self.videoButton.selected = YES;
        self.videoTimeLabel.hidden = NO;
        self.videoTimeLabel.text = @"00:00";
        self.recordingSeconds = 0;
        
        __unsafe_unretained MainViewController *weakSelf = self;
        _recording_timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_current_queue());
        dispatch_source_set_timer(_recording_timer,
                                  dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC),
                                  1.0 * NSEC_PER_SEC, 0);
        dispatch_source_set_event_handler(_recording_timer, ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelf.recordingSeconds++;
                weakSelf.videoTimeLabel.text = [NSString stringWithFormat:@"%.2i:%.2i",
                                                weakSelf.recordingSeconds / 60,
                                                weakSelf.recordingSeconds % 60];
                weakSelf.videoButton.selected = !weakSelf.videoButton.selected;
            });
        });
        
        //start it
        dispatch_resume(_recording_timer);
        
        [self.videoPainter startRecording];
    } else {
        //finish recording
        self.recording = NO;
        self.cameraButton.hidden = NO;
        dispatch_source_cancel(_recording_timer);
        self.videoButton.selected = NO;
        self.videoTimeLabel.hidden = YES;
        
        __unsafe_unretained MainViewController *weakSelf = self;
        [PainterUtility popupWithStatus:@"Saving Video"
                                  queue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
                                  block:^{
                                      [weakSelf.videoPainter finishRecordingWithBlock:^(NSURL *moviePath) {
                                          [PainterUtility saveVideoToPhotoAlbum:moviePath];
                                      }];
                                  }];
    }
}

- (IBAction)addLayerClicked:(id)sender; {
    if (![self.imagePainter canAppendNewLayer]) return;
    
    [self.imagePainter appendNewLayer];
    BOOL hasSeenLayerDirections = [[SettingsModel getInstance] hasSeenLayerDirections];
    
    if (!hasSeenLayerDirections) {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"New Layer Created"
                                                          message:@"A new color layer has been created.\n Changes in color will not affect the existing paint below it.\n\n Feel free to select a new color and paint either new walls or existing areas."
                                                         delegate:self
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        [[SettingsModel getInstance] setHasSeenLayerDirections:YES];
    } else {
        [SVProgressHUD showSuccessWithStatus:@"New Layer Created"];
    }
    self.addLayerButton.enabled = NO;
}

- (void)cancelScheduledCommit
{
    if (self.commitTimer && !self.commitTimingOut) {
        dispatch_source_cancel(self.commitTimer);
        self.commitTimer = 0;
    }
}

- (void)scheduleCommit
{
    [self cancelScheduledCommit];
    
    __unsafe_unretained MainViewController *weakSelf = self;
    self.commitTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0));
    dispatch_source_set_timer(self.commitTimer,
                              dispatch_time(DISPATCH_TIME_NOW, AUTOCOMMIT_SECONDS * NSEC_PER_SEC),
                              DISPATCH_TIME_FOREVER, 0);
    dispatch_source_set_event_handler(self.commitTimer, ^{
        self.commitTimingOut = YES;
        dispatch_source_cancel(self.commitTimer);
        self.commitTimer = 0;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.imagePainter commitChanges];
            self.commitTimingOut = NO;
        });
    });
    
    //start it
    dispatch_resume(self.commitTimer);
}

- (IBAction)swatchesButtonClicked:(id)sender {    
    if (self.pickerMode == PickerModePicker) {
        [self openPicker:![self pickerIsOpen]];
    } else if (self.pickerMode == PickerModeSwatches) {
        [self openSwatches:![self swatchesIsOpen]];
    } else if (self.pickerMode == PickerModeFavorites) {
        [self openFavorites:![self favoritesIsOpen]];
    }
}

- (IBAction)videoButtonClicked:(id)sender {
    [self toggleVideoRecording:!self.recording];
}

- (IBAction)cameraButtonClicked:(id)sender {
    
    [self.imagePainter clearHistory];
    
    [self.videoPainter captureCurrentState:^(CBImage *image) {
        
        self.imagePainter.stillImage = image;
        //self.stillImageView.image = self.imagePainter.stillImage.image;
        [self toggleStillMode:YES];
    }];
}


- (IBAction)backButtonClicked:(id)sender {
    //ask to save
    if (self.unsavedWork) {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:SAVE_CHANGES_ALERT
                                                          message:@"Changes have been made without saving. Would you like to save your changes?"
                                                         delegate:self
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:@"No", nil];
        [message show];
    } else {
        [self leaveStillMode];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
{
    if ([alertView.title isEqualToString:SAVE_CHANGES_ALERT]) {
        if (buttonIndex == 0) {
            [self saveStillClicked:self];
        }
        
        [self leaveStillMode];
    }
}

- (void) leaveStillMode
{
    [self.imagePainter clearHistory];
    [self toggleStillMode:NO];
}

#pragma mark -
#pragma mark Delegate Methods

- (void) SideBar_ShowLibrary;
{
    [self openSwatches:NO];
    self.loadingSubview = YES;
    [self.drawer toggleDrawer:NO];
    
    UIImagePickerController *libraryPicker = [[UIImagePickerController alloc] init];
    libraryPicker.delegate = self;
    libraryPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    if (IS_IPAD) {
        _popover = [[UIPopoverController alloc] initWithContentViewController:libraryPicker];
        _popover.delegate = self;
        CGRect centerRect = CGRectCenter(CGRectMake(1, 1, 1, 1), self.view.frame);
        [_popover presentPopoverFromRect:centerRect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    } else {
        [libraryPicker setModalPresentationStyle:UIModalPresentationPageSheet];
        [self presentViewController:libraryPicker animated:YES completion:^{
            self.loadingSubview = NO;
        }];
    }

}

- (void) SideBar_PickedColorCategory:(ColorCategory *)colorCategory;
{
    [self loadCategory:colorCategory];
    self.pickerMode = PickerModeSwatches;
    
    [self openSwatches:YES];
    [self.drawer toggleDrawer:NO];    
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController;
{
    return YES;
}

/* Called on the delegate when the user has taken action to dismiss the popover. This is not called when -dismissPopoverAnimated: is called directly.
 */
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController;
{
    self.loadingSubview = NO;
}

- (void)imagePickerController:(UIImagePickerController *)pker
didFinishPickingMediaWithInfo:(NSDictionary *)info;
{
    UIImage *largeImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    [pker dismissModalViewControllerAnimated:NO];
    if (IS_IPAD && _popover) {
        [_popover dismissPopoverAnimated:YES];
    }
    
    [self.imagePainter loadImage:largeImage hasAlphaMasking:NO];
    [self toggleStillMode:YES];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker;
{
    if (IS_IPAD && _popover) {
        [_popover dismissPopoverAnimated:YES];
    } else {
        [picker dismissModalViewControllerAnimated:YES];
    }
    [self.videoPainter startRunning];
}

- (void) SideBar_OpenedColorPicker;
{
    [self openSwatches:NO];
    self.pickerMode = PickerModePicker;
    [self openPicker:YES];
    [self.drawer toggleDrawer:NO];
}

- (void) SideBar_OpenedHarmony:(BOOL)isCamera;
{
    [self.videoPainter stopRunning];
    self.loadingSubview = YES;
    [self.drawer toggleDrawer:NO];
    
    double delayInSeconds = IS_IPHONE_5 ? 0.1 : 0.5;
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        HarmonyViewController *harmony = [[HarmonyViewController alloc] initWithNibName:nil bundle:nil];
        harmony.delegate = self;
        harmony.parent = self;
        harmony.color = self.color;
        
        harmony.modalPresentationStyle = UIModalPresentationPageSheet;
        harmony.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        
        [self presentViewController:harmony animated:NO completion:^{
            if (isCamera) {
                [harmony showCamera:YES];
            } else {
                [harmony showLibrary:NO];
            }
        }];
    });
    
}

- (void) SideBar_ShowFavorites;
{
    [self.drawer toggleDrawer:NO];
    
    if (![[PainterUtility favorites] count]) {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:FAVORITES_ALERT
                                                          message:@"You do not currently have any favorites. To add a favorite, click on a color in the top righthand corner and then click the star on the color details page."
                                                         delegate:self
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
    } else {
        [self refreshFavorites];
        self.pickerMode = PickerModeFavorites;
        [self openFavorites:YES];
    }
}

- (void) SideBar_PickedColor:(Color *)color;
{
    self.color = color;
    self.pickerMode = PickerModeSwatches;
    [self openSwatches:YES];
    
    [self.drawer toggleDrawer:NO];
}

- (void) SideBar_EmailMore;
{
    __unsafe_unretained MainViewController *weakSelf = self;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        if (IS_IPAD) {
            mailer.modalPresentationStyle = UIModalPresentationPageSheet;
        }
        mailer.mailComposeDelegate = weakSelf;
        [mailer setSubject:@"Product Recomendation"];
        
        [mailer setToRecipients:[NSArray arrayWithObject:@"video-painter@cambriantech.com"]];
        
        [weakSelf presentModalViewController:mailer animated:YES];
    });
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    // Remove the mail view
    [self dismissModalViewControllerAnimated:YES];
}

- (void)ColorViewController_colorClicked:(Color *)color;
{
    self.color = color;
    [self displayColorDetails:color cameFromSelf:NO];
}


- (void) SelectColor_PickedColor:(Color *)color uiColor:(UIColor*)uiColor;
{
    self.color = color;
    
    [self setUIColor:uiColor];
}

- (void)colorPickerViewController:(FCColorPickerViewController *)colorPicker colorChanged:(UIColor *)uiColor;
{
    if (self.uiColor == uiColor || self.pickerMode != PickerModePicker) {
        return;
    }
    
    self.swatchesColorView.backgroundColor = uiColor;
    if ([[SettingsModel getInstance] matchPickerColors]) {
        [self locatePaintColor:uiColor];
    } else {
        self.color = nil;
        [self setUIColor:uiColor];
    }
}

- (void)colorPickerViewControllerSwatchClicked;
{
    [self openPicker:NO];
}

- (void)ColorScroller_titleClicked:(ColorScroller *)colorScroller;
{
    [self openFavorites:NO];
}

- (void)ColorScroller_colorClicked:(ColorScroller *)colorScroller color:(id)color;
{
    self.color = color;
}

//- (void)colorPickerViewController:(FCColorPickerViewController *)colorPicker colorIdle:(UIColor *)color;
//{
//    
//}

static BOOL busyUpdatingColor;
- (void) locatePaintColor:(UIColor *)uiColor
{
    if (busyUpdatingColor) return;
    
    if (_color_finder_timer) {
        dispatch_source_cancel(_color_finder_timer);
        _color_finder_timer = 0;
    }
    
    __unsafe_unretained MainViewController *weakSelf = self;
    
    _color_finder_timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_current_queue());
    dispatch_source_set_timer(_color_finder_timer,
                              dispatch_time(DISPATCH_TIME_NOW, 0.25 * NSEC_PER_SEC),
                              DISPATCH_TIME_FOREVER, 0);
    dispatch_source_set_event_handler(_color_finder_timer, ^{
        
        busyUpdatingColor = YES;
        
        dispatch_source_cancel(_color_finder_timer);
        _color_finder_timer = 0;
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            Color *color = [PainterUtility closestMatchForUIColor:uiColor];
            NSLog(@"Closest color is %@", color.name);
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelf.color = color;
                busyUpdatingColor = NO;
            });
        });
        
    });
    
    //start it
    dispatch_resume(_color_finder_timer);

}

- (IBAction)brandCheckboxClicked:(id)sender {
    BOOL existing = [[SettingsModel getInstance] matchPickerColors];
    [[SettingsModel getInstance] setMatchPickerColors:!existing];
    self.brandCheckbox.selected = !existing;
}

- (IBAction)saveStillClicked:(id)sender
{
    self.unsavedWork = NO;
    __unsafe_unretained MainViewController *weakSelf = self;
    [PainterUtility popupWithStatus:@"Saving Photo"
                              queue:dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
                              block:^{
                                  [PainterUtility saveImageToPhotoAlbum:weakSelf.imagePainter.previewImage];
                              }];
}

#pragma mark -
#pragma mark Button Actions

- (IBAction)undoClicked:(id)sender;
{
    [self.imagePainter stepBackward];
}

- (IBAction)paintClicked:(id)sender {
    self.imagePainter.toolMode = ToolModePaintbrush;
    [self updateTools];
}

- (IBAction)brushSizeSliderChanged:(id)sender {
    
    if (self.imagePainter.toolMode == ToolModePaintbrush || self.imagePainter.toolMode == ToolModeEraser) {
        self.imagePainter.brushSize = self.brushSizeSlider.value;
        self.brushSizeImage.hidden = NO;
        __unsafe_unretained MainViewController *weakSelf = self;
                 [PainterUtility performBlock:^{
                     weakSelf.brushSizeImage.hidden = YES;
        } delayedBy:0.5 onQueue:dispatch_get_main_queue() usingTimer:&_block_timer];
    }
    
    [self updateBrushSize];
}

- (void)updateBrushSize;
{
    if (!self.imagePainter.previewImage) return;
    
    float widthScale = self.imagePainter.bounds.size.width / self.imagePainter.previewImage.size.width;
    float heightScale = self.imagePainter.bounds.size.height / self.imagePainter.previewImage.size.height;
    float scale = widthScale < heightScale ? widthScale : heightScale;
    
    float effectiveSize = 2.3 * self.imagePainter.brushSize * scale;

    CGPoint previousOrigin = self.brushSizeImage.frame.origin;
    CGSize previousSize = self.brushSizeImage.frame.size;
    self.brushSizeImage.frame = CGRectSetSize(CGSizeMake(effectiveSize, effectiveSize), self.brushSizeImage.frame);
    
    self.brushSizeImage.frame = CGRectSetY(previousOrigin.y - (self.brushSizeImage.frame.size.height - previousSize.height) / 2, self.brushSizeImage.frame);
    
    self.brushSizeImage.frame = CGRectSetX(previousOrigin.x - (self.brushSizeImage.frame.size.width - previousSize.width) / 2, self.brushSizeImage.frame);
}


- (IBAction)eraseClicked:(id)sender {
    self.imagePainter.toolMode = ToolModeEraser;
    [self updateTools];
}

- (IBAction)paintBucketClicked:(id)sender {
    self.imagePainter.toolMode = ToolModeFill;
    [self updateTools];
}

- (IBAction)debugSwitchChanged:(id)sender {
    self.videoPainter.debugMode = self.debugSwitch.on;
}

- (void)updateTools
{
    self.paintButton.selected = self.imagePainter.toolMode == ToolModePaintbrush;
    self.eraseButton.selected = self.imagePainter.toolMode == ToolModeEraser;
    self.bucketButton.selected = self.imagePainter.toolMode == ToolModeFill;
    self.brushSizeImage.hidden = YES;
    
    self.brushSizeSlider.maximumValue = self.imagePainter.previewImage.size.width / 10;
    self.brushSizeSlider.minimumValue = 1 + self.brushSizeSlider.maximumValue / 10;
    self.imagePainter.brushSize = (self.brushSizeSlider.maximumValue + self.brushSizeSlider.minimumValue) / 2;
    self.brushSizeSlider.value = self.imagePainter.brushSize;
    
    self.brushSizeSlider.hidden = self.imagePainter.toolMode == ToolModeFill;
    
    [self updateBrushSize];
    
    self.undoButton.enabled = self.imagePainter.canStepBackward;
}

#pragma mark -
#pragma mark Ads

- (void)bannerViewDidLoadAd:(ADBannerView *)banner;
{
    if (self.banner.hidden) {
        self.banner.hidden = NO;
        _bannerHeight = self.banner.frame.size.height;
        _controlsOrigin = CGPointMake(_controlsOrigin.x, _controlsOrigin.y - _bannerHeight);
        
        [UIView animateWithDuration:0.3 animations:^{
            [self controlsOriginChanged];
            self.banner.frame = CGRectSetY(self.videoView.frame.size.height - self.banner.frame.size.height,
                                           self.banner.frame);
            
            if ([self swatchesIsOpen]) [self openSwatches:YES];
            else if ([self pickerIsOpen]) [self openPicker:YES];
            else if ([self favoritesIsOpen]) [self openFavorites:YES];
            
        } completion:^(BOOL finished) {

        }];
    }
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error;
{
    NSLog(@"Didn't load ad");
}

- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave;
{
    
    return YES;
}

- (void)bannerViewActionDidFinish:(ADBannerView *)banner;
{
    
}

#pragma mark -
#pragma mark Drawer

- (void) CBDrawerController_drawerWillOpen:(id)drawer;
{
    self.loadingSubview = NO;
    [self.sideDrawer sidebarWillAppear];
}

- (void) CBDrawerController_drawerWillClose:(id)drawer;
{
    [self.sideDrawer sidebarWillDisappear];
}

- (void) CBDrawerController_drawerDidOpen:(id)drawer;
{
    if (self.videoPainter.isRunning) {
        [self.videoPainter stopRunning];
    }
}

- (void) CBDrawerController_drawerDidClose:(id)drawer;
{
    if (!self.loadingSubview && ![self isStillMode]) {
        [self.videoPainter startRunning];
    }
}

@end
