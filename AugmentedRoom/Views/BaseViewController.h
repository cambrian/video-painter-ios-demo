//
//  BaseViewController.h
//  AugmentedRoom
//
//  Created by Joel Teply on 10/25/12.
//  Copyright (c) 2012 Joel Teply. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

- (id)init;

- (void) autolayoutCompleted;

@end
