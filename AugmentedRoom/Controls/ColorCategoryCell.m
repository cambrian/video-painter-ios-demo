//
//  ColorCategoryCell.m
//  AugmentedRoom
//
//  Created by Joel Teply on 10/23/12.
//  Copyright (c) 2012 Joel Teply. All rights reserved.
//

#import "ColorCategoryCell.h"
#import "ColorCategory.h"
#import "Brand.h"

@interface ColorCategoryCell()

@property (retain, nonatomic) IBOutlet UILabel *brandLabel;
@property (retain, nonatomic) IBOutlet UILabel *categoryLabel;
@property (retain, nonatomic) IBOutlet UIImageView *brandIcon;

@end

@implementation ColorCategoryCell

@synthesize colorCategory = _colorCategory;
@synthesize brandIcon = _brandIcon;
@synthesize categoryLabel = _categoryLabel;
@synthesize brandLabel = _brandLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setColorCategory:(ColorCategory *)colorCategory;
{
    _colorCategory = colorCategory;
    
    self.brandLabel.text = colorCategory.brand.name;
    self.brandLabel.textColor = UICOLOR_SIDEBAR_ITEM_TEXT;
    
    self.brandIcon.image = [UIImage imageNamed:colorCategory.brand.icon];
    self.categoryLabel.text = colorCategory.name;
    self.categoryLabel.textColor = UICOLOR_SIDEBAR_ITEM_TEXT;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
