//
//  PainterUtility.h
//  VirtualPainter
//
//  Created by Joel Teply on 11/7/12.
//  Copyright (c) 2012 Joel Teply. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <HomeAugmentation/CBColoring.h>

#define FavoritesChangedNotification @"FavoritesChangedNotification"

@class Brand, ColorCategory, Color;

@interface PainterUtility : NSObject

+ (UIImageView *)makeColorTexture:(UIImage *)texture withColor:(UIColor *)color withFrame:(CGRect)frame;

+ (UIImage *)makeColorTextureImage:(UIImage *)texture withColor:(UIColor *)color withFrame:(CGRect)frame;

+ (BOOL)isTouch:(UITouch *)touch withinView:(UIView *)view;

+ (NSArray *) allBrands;
+ (NSArray *) allColors;

+ (Color *)closestMatchForUIColor:(UIColor *)uiColor;
+ (Color *)closestMatchForUIColor:(UIColor *)uiColor brand:(Brand*)brand category:(ColorCategory*)category;

+ (Color *)closestMatchForUIColor:(UIColor *)uiColor
                     withinColors:(id<NSFastEnumeration>)colors
                  excludingColors:(NSArray *)colorsToExclude;

//+ (void)gravityForViews:(NSArray *)views isReversed:(BOOL)upIsDown ignoreView:(UIView *)ignoreView;

+ (void) saveImageToPhotoAlbum:(UIImage *)image;

+ (void) saveVideoToPhotoAlbum:(NSURL *)url;

+ (NSMutableArray *) favorites;

+ (void)popupWithStatus:(NSString *)status queue:(dispatch_queue_t)queue block:(void (^)())block;

+ (void) performBlock:(void (^)(void))block
            delayedBy:(double)delay
              onQueue:(dispatch_queue_t)queue
           usingTimer:(dispatch_source_t *)timer;

+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL;

+ (void)setFavoriteColor:(Color *)color isFavorite:(BOOL)isFavorite;

+ (NSString *)appStoreHTML;

+ (UIImageView *) setupTutorialStep:(UIView *)view
                              above:(BOOL)above
                           centered:(BOOL)centered;

@end
