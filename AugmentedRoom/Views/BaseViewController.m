//
//  BaseViewController.m
//  AugmentedRoom
//
//  Created by Joel Teply on 10/25/12.
//  Copyright (c) 2012 Joel Teply. All rights reserved.
//

#import "BaseViewController.h"
#import "SwatchesViewController.h"
#import "ColorViewController.h"

@interface BaseViewController () {
    BOOL _bHideStatusBar;
    dispatch_source_t _statusBarWatcherTimer;
    BOOL _isFirstRun;
}

@end

@implementation BaseViewController

- (id)init
{
    return [self initWithNibName:nil bundle:nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    NSString *baseNibName = NSStringFromClass([self class]);
    
    if (!nibNameOrNil) {
        if (IS_IPAD) {
            NSString *ipadFilename = [NSString stringWithFormat:@"%@_iPad", baseNibName];
            if ([[NSBundle mainBundle] pathForResource:ipadFilename ofType:@"nib"]) {
                nibNameOrNil = ipadFilename;
            }
            
        } else {
            NSString *iphoneFilename = [NSString stringWithFormat:@"%@_iPhone", baseNibName];
            if ([[NSBundle mainBundle] pathForResource:iphoneFilename ofType:@"nib"]) {
                nibNameOrNil = iphoneFilename;
            }
        }
    }
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _isFirstRun = YES;
	// Do any additional setup after loading the view.
}

- (void)dispatchStatusBarToHell:(BOOL)hide
{
    _bHideStatusBar = hide;
    
    int frequencySeconds = 0.5;
    
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
        _statusBarWatcherTimer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue());
        dispatch_source_set_timer(_statusBarWatcherTimer,
                                  dispatch_time(DISPATCH_TIME_NOW, frequencySeconds * NSEC_PER_SEC),
                                  frequencySeconds, 0);
        dispatch_source_set_event_handler(_statusBarWatcherTimer, ^{
            [[UIApplication sharedApplication] setStatusBarHidden:_bHideStatusBar withAnimation:UIStatusBarAnimationNone];
            [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
        });
        dispatch_resume(_statusBarWatcherTimer);
    } else {
        [[UIApplication sharedApplication] setStatusBarHidden:_bHideStatusBar withAnimation:UIStatusBarAnimationNone];
    }
}

- (BOOL)prefersStatusBarHidden
{
    return _bHideStatusBar;
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self dispatchStatusBarToHell:YES];
    if (_isFirstRun) {
        [self autolayoutCompleted];
        _isFirstRun = NO;
    }
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (_statusBarWatcherTimer) {
        dispatch_source_cancel(_statusBarWatcherTimer);
        _statusBarWatcherTimer = 0;
    }
}

- (void) autolayoutCompleted
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
