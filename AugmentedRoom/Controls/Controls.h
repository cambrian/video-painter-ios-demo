//
//  Controls.h
//  VideoPainter
//
//  Created by Joel Teply on 2/12/13.
//  Copyright (c) 2013 Joel Teply. All rights reserved.
//

#ifndef VideoPainter_Controls_h
#define VideoPainter_Controls_h

#import "Color.h"

@protocol SelectColorDelegate <NSObject>

@required

- (void) SelectColor_PickedColor:(Color *)color uiColor:(UIColor*)uiColor;

@end


#endif
