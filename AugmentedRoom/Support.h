//
//  Support.h
//  Paint Harmony
//
//  Created by Joel Teply on 3/31/14.
//
//

#ifndef Paint_Harmony_Support_h
#define Paint_Harmony_Support_h

#ifndef __IPHONE_4_0
#warning "This project uses features only available in iOS SDK 4.0 and later."
#endif

#ifdef __OBJC__
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <HomeAugmentation/HomeAugmentation.h>
#import "BaseViewController.h"
#endif

#define APP_NAME @"Paint Harmony"
#define PRO_URL @"https://itunes.apple.com/us/app/paint-harmony/id616218410?ls=1&mt=8"
#define CONTACT_EMAIL @"paint-harmony@cambriantech.com"
#define INITIAL_IMAGE @"bedroom.png"
#define DATABASE_NAME @"ColorModel.sqlite"
#define INITIAL_COLOR_CATEGORY @"ColorSmart Premium Plus"

#include <sys/sysctl.h>
static inline unsigned int countCores()
{
    size_t len;
    unsigned int ncpu;
    
    len = sizeof(ncpu);
    sysctlbyname ("hw.ncpu",&ncpu,&len,NULL,0);
    
    return ncpu;
}

#define HAS_CAMERA ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerSourceTypeCamera])

#define IS_RETINA ([[UIScreen mainScreen] scale] > 1.0)
#define IS_MULTICORE (countCores() > 1)
#define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

#define IS_IPHONE ( [ [ [ UIDevice currentDevice ] model ] isEqualToString: @"iPhone" ] )
#define IS_IPOD   ( [ [ [ UIDevice currentDevice ] model ] isEqualToString: @"iPod touch" ] )
#define IS_IPHONE_5 ( IS_IPHONE && IS_WIDESCREEN )
#define IS_IPAD ( [[[ UIDevice currentDevice ] model] hasPrefix:@"iPad"] )

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


#define UICOLOR_TOOL_FONT_COLOR [UIColor colorWithRed:84.0/255.0 green:75.0/255.0 blue:87.0/255.0 alpha:1]
#define UICOLOR_TOOL_BG [UIColor colorWithRed:24.0/255.0 green:25.0/255.0 blue:27.0/255.0 alpha:1]

#define UICOLOR_SIDEBAR_ITEM UIColorFromRGB(0x3a3a3b)
#define UICOLOR_SIDEBAR_ITEM_SELECTED UIColorFromRGB(0x2b2c2f)
#define UICOLOR_SIDEBAR_ITEM_TEXT UIColorFromRGB(0xb6becb)

#define UICOLOR_SIDEBAR_HEADER_TEXT UICOLOR_SIDEBAR_ITEM_TEXT

#define UICOLOR_SIDEBAR_BG UIColorFromRGB(0x161719)

#define DEFAULT_COLOR UIColorFromRGB(0xc0fce0)

#endif
