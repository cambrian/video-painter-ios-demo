//
//  ColorCategoryCell.m
//  AugmentedRoom
//
//  Created by Joel Teply on 10/23/12.
//  Copyright (c) 2012 Joel Teply. All rights reserved.
//

#import "IconBasedCell.h"

@interface IconBasedCell()


@end

@implementation IconBasedCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
    }
    return self;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    self.headingLabel.textColor = UICOLOR_SIDEBAR_ITEM_TEXT;
    self.subheadingLabel.textColor = UICOLOR_SIDEBAR_ITEM_TEXT;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
