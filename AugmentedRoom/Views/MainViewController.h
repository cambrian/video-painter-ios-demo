//
//  ViewController.h
//  AugmentedRoom
//
//  Created by Joel Teply on 10/12/12.
//  Copyright (c) 2012 Joel Teply. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Controls.h"
#import "SideBarViewController.h"
#import "ColorViewController.h"
#import "FCColorPickerViewController.h"
#import <iAd/iAd.h>
#import <MessageUI/MessageUI.h>
#import "ImagePainter.h"

@interface MainViewController : BaseViewController <CBDrawerControllerDelegate, SelectColorDelegate, SideBarDelegate,
    ColorViewControllerDelegate, ColorPickerViewControllerDelegate, UIAlertViewDelegate,
    ColorScrollerDelegate, ADBannerViewDelegate, MFMailComposeViewControllerDelegate,
    UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate>
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *topBarImage;

@property (retain, nonatomic) IBOutlet UIView *videoView;
@property (strong, nonatomic) IBOutlet ImagePainter *imagePainter;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *videoOutput;
@property (retain, nonatomic) IBOutlet UIView *topControls;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *cameraControls;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *stillControls;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *cameraButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *videoButton;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *videoTimeLabel;
@property (weak, nonatomic) IBOutlet UISwitch *debugSwitch;

@property (retain, nonatomic) IBOutlet UILabel *statusLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *closeButton;

@property (retain, nonatomic) IBOutlet UIView *colorButtonView;

@property (retain, nonatomic) IBOutlet UIView *swatchesContainer;
@property (retain, nonatomic) IBOutlet UIView *swatchesColorView;
@property (retain, nonatomic) IBOutlet UIImageView *swatchesArrowUp;
@property (retain, nonatomic) IBOutlet UIImageView *swatchesArrowDown;


@property (unsafe_unretained, nonatomic) IBOutlet UIView *pickerInfoView;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *pickerInfoLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *brandCheckbox;
@property (retain, nonatomic) IBOutlet UILabel *colorLabel;

@property (unsafe_unretained, nonatomic) IBOutlet UIButton *undoButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *addLayerButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *paintButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *eraseButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *bucketButton;
@property (unsafe_unretained, nonatomic) IBOutlet UISlider *brushSizeSlider;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *brushSizeImage;

@property (weak, nonatomic) IBOutlet UIView *floodOptions;
@property (weak, nonatomic) IBOutlet UISlider *floodSlider;

- (IBAction)commitFlood:(id)sender;
- (IBAction)decommitFlood:(id)sender;
- (IBAction)floodChanged:(id)sender;
- (IBAction)floodSliderClicked:(id)sender;
- (IBAction)addLayerClicked:(id)sender;

- (IBAction)drawerClicked:(id)sender;
- (IBAction)colorButtonClicked:(id)sender;

- (IBAction)swatchesButtonClicked:(id)sender;
- (IBAction)videoButtonClicked:(id)sender;
- (IBAction)cameraButtonClicked:(id)sender;
- (IBAction)backButtonClicked:(id)sender;

- (IBAction)brandCheckboxClicked:(id)sender;

- (IBAction)saveStillClicked:(id)sender;
- (IBAction)undoClicked:(id)sender;
- (IBAction)paintClicked:(id)sender;
- (IBAction)brushSizeSliderChanged:(id)sender;
- (IBAction)eraseClicked:(id)sender;
- (IBAction)paintBucketClicked:(id)sender;
- (IBAction)debugSwitchChanged:(id)sender;

@end
